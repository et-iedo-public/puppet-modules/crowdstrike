# crowdstrike - CrowdStrike (falcon-sensor) for Endpoint Monitoring

This is a puppet module that deploys the CrowdStrike Agent.

# Prerequisites

* This module assumes that the CrowdStrike Agent (falcon-sensor) package is available via external repo (http://debian.stanford.edu/debian-stanford)

# Puppet Module Usage
Simply include this module in the puppet code
```bash
include crowdstrike
```

Or Puppet Module Parameters can be specified in this way:
```bash
class { 'crowdstrike':
  ensure => 'absent',
}
```

And Puppet Module Parameters can also be specified via Hiera in the following format:
```bash
crowdstrike::cid:    'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-xx'
crowdstrike::tags:   'UIT,ET'
```

## Puppet Module Parameters

### ensure (Enum) - default: 'present'
Determines if crowdstrike is installed or uninstalled from the system.
```bash
crowdstrike::ensure:     ['absent', 'present']
```

### cid (String) - detault: undef
Customer Identification is a code provided by the CrowdSrike Administrator (ISO) to activate the CrowdStrike Agent

### tags (String) - detault: undef
tags allow for the categorization of the host within the CrowdStrike Console.  Please check with the CrowdStrike Administrator (ISO) for which tags should be used
