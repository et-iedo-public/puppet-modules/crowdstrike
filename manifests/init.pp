class crowdstrike (
  Enum['absent', 'present'] $ensure = 'present',
  String $cid                       = '',
  String $tags                      = '',
) {
# Description:: Falcon sensor (CrowdStrike) agent install and config

  if ($ensure == 'present') {
    exec {'register_cid':
      command     => "/opt/CrowdStrike/falconctl -s -f --cid=$cid --tags=\"$tags\"",
      refreshonly => 'true',
    }

    package { 'falcon-sensor':
      ensure => 'present',
      notify => Exec['register_cid'],
    }

    service {'falcon-sensor':
      ensure    => 'running',
      enable    => 'true',
      subscribe => Exec['register_cid'],
    }
  } else {
    package { 'falcon-sensor':
      ensure => 'absent',
    }
    service {'falcon-sensor':
      ensure    => 'stopped',
      enable    => 'false',
    }
  }
}
